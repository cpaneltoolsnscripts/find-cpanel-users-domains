#!/bin/bash
## Author: Michael Ramsey
## https://gitlab.com/mikeramsey/find-cpanel-users-domains
## Objective Find A cPanel User's accounts and all of their domains for cPanel
## How to use.
## ./find_users_domains.sh username

sudo grep ": $1" /etc/userdomains | cut -d: -f1